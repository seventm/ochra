package ochra

import (
	"fmt"
)

// Version: 0.0.2

// Font colors
func Yellow(txt string) string {
	return MakeFontColor(txt, "yellow")
}

func Blue(txt string) string {
	return MakeFontColor(txt, "blue")
}

func Red(txt string) string {
	return MakeFontColor(txt, "red")
}

func Cyan(txt string) string {
	return MakeFontColor(txt, "cyan")
}

func Green(txt string) string {
	return MakeFontColor(txt, "green")
}

func Magenta(txt string) string {
	return MakeFontColor(txt, "magenta")
}

func Black(txt string) string {
	return MakeFontColor(txt, "black")
}

func White(txt string) string {
	return MakeFontColor(txt, "white")
}

func Silver(txt string) string {
	return MakeFontColor(txt, "silver")
}

// Backgound colors
func BGRed(txt string) string {
	return MakeBackColor(txt, "red")
}

func BGGreen(txt string) string {
	return MakeBackColor(txt, "green")
}

func BGCyan(txt string) string {
	return MakeBackColor(txt, "cyan")
}

func BGBlue(txt string) string {
	return MakeBackColor(txt, "blue")
}

func BGYellow(txt string) string {
	return MakeBackColor(txt, "yellow")
}

func BGMagenta(txt string) string {
	return MakeBackColor(txt, "magenta")
}

func BGWhite(txt string) string {
	return MakeBackColor(txt, "white")
}

func BGBlack(txt string) string {
	return MakeBackColor(txt, "black")
}

// style
func Bold(txt string) string {
	return MakeFontBold(txt)
}

func Italic(txt string) string {
	return MakeFontItalic(txt)
}

func Underline(txt string) string {
	return MakeFontUnderline(txt)
}

func Inverse(txt string) string {
	return MakeFontInverse(txt)
}

func Strike(txt string) string {
	return MakeFontStrike(txt)
}

func Hidden(txt string) string {
	return MakeFontHidden(txt)
}

// Make background color
// arg: txt string
// arg: color color name black, red, green, yellow, blue, magenta, cyan, white, silver
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeBackColor(txt string, color string) string {
	style_code := BackColorCode(color)
	return fmt.Sprintf(
		"%s%s%s",
		style(style_code),
		txt,
		closeBackColor())
}

// Make font color
// arg: txt string
// arg: color color name black, red, green, yellow, blue, magenta, cyan, white, silver
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontColor(txt string, color string) string {
	style_code := FontColorCode(color)
	return fmt.Sprintf(
		"%s%s%s",
		style(style_code),
		txt,
		closeFontColor())
}

// Make font bold
// arg: txt string
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontBold(txt string) string {
	return fmt.Sprintf(
		"%s%s%s",
		style(1),
		txt,
		closeStyleBold())
}

// Make font italic
// arg: txt string
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontItalic(txt string) string {
	return fmt.Sprintf(
		"%s%s%s",
		style(3),
		txt,
		closeStyleItalic())
}

// Make font underline
// arg: txt string
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontUnderline(txt string) string {
	return fmt.Sprintf(
		"%s%s%s",
		style(4),
		txt,
		closeStyleUnderline())
}

// Make font inverse
// arg: txt string
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontInverse(txt string) string {
	return fmt.Sprintf(
		"%s%s%s",
		style(7),
		txt,
		closeStyleInverse())
}

// Make font strike
// arg: txt string
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontStrike(txt string) string {
	return fmt.Sprintf(
		"%s%s%s",
		style(9),
		txt,
		closeStyleStrike())
}

// Make font hidden
// arg: txt string
// return: txt string flagged with style codes
// author: Konrad J. Debski
func MakeFontHidden(txt string) string {
	return fmt.Sprintf(
		"%s%s%s",
		style(8),
		txt,
		closeStyleHidden())
}

// close font color style
// return: \033[39m
// author: Konrad J. Debski
func closeFontColor() string {
	return style(39)
}

// close background color style
// return: \033[49m
// author: Konrad J. Debski
func closeBackColor() string {
	return style(49)
}

// close font style bold
// return: \033[22m
// author: Konrad J. Debski
func closeStyleBold() string {
	return style(22)
}

// close font style italic
// return: \033[23m
// author: Konrad J. Debski
func closeStyleItalic() string {
	return style(23)
}

// close font style italic
// return: \033[24m
// author: Konrad J. Debski
func closeStyleUnderline() string {
	return style(24)
}

// close font style inverse
// return: \033[27m
// author: Konrad J. Debski
func closeStyleInverse() string {
	return style(27)
}

// close font style strike
// return: \033[29m
// author: Konrad J. Debski
func closeStyleStrike() string {
	return style(29)
}

// close font style hidden
// return: \033[28m
// author: Konrad J. Debski
func closeStyleHidden() string {
	return style(28)
}

// style string
// arg: code style code
// return: style string i.e. \033[%dm
// author: Konrad J. Debski
func style(code int) string {
	return fmt.Sprintf("\033[%dm", code)
}

// convert color to int code (font)
// arg: color color name black, red, green, yellow, blue, magenta, cyan, white, silver
// return: int color code e.g. 31 for red
// author: Konrad J. Debski
func FontColorCode(color string) int {
	color_codes := map[string]int{
		"black":   30,
		"red":     31,
		"green":   32,
		"yellow":  33,
		"blue":    34,
		"magenta": 35,
		"cyan":    36,
		"white":   37,
		"silver":  90,
	}
	return color_codes[color]
}

// convert color to int code (background)
// arg: color color name black, red, green, yellow, blue, magenta, cyan, white, silver
// return: int color code e.g. 31 for red
// author: Konrad J. Debski
func BackColorCode(color string) int {
	color_codes := map[string]int{
		"black":   40,
		"red":     41,
		"green":   42,
		"yellow":  43,
		"blue":    44,
		"magenta": 45,
		"cyan":    46,
		"white":   47,
	}
	return color_codes[color]
}
